import 'package:flutter/material.dart';
import 'package:test_interview/Home/HomePage.dart';
import 'package:test_interview/Object/ColorObject/ColorObject.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.amber[600],
        accentColor: Colors.grey[600],
        primaryTextTheme: TextTheme(
          headline1: TextStyle(color:Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
          headline2: TextStyle(color:Colors.white, fontSize: 12.0, fontWeight: FontWeight.bold),
          headline3: TextStyle(color:Colors.black, fontSize: 12.0, fontWeight: FontWeight.bold),
        ),
      ),
      home: HomePage(),
    );
  }
}

