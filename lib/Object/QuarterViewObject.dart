import 'dart:convert';

QuarterViewObject quarterViewObjectFromJson(String str) => QuarterViewObject.fromJson(json.decode(str));

String quarterViewObjectToJson(QuarterViewObject data) => json.encode(data.toJson());

class QuarterViewObject {
    QuarterViewObject({
        this.code,
        this.msg,
        this.data,
    });

    int code;
    String msg;
    Data data;

    factory QuarterViewObject.fromJson(Map<String, dynamic> json) => QuarterViewObject(
        code: json["code"],
        msg: json["msg"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "msg": msg,
        "data": data.toJson(),
    };
}

class Data {
    Data({
        this.list,
        this.summarytier,
        this.summary,
        this.total,
    });

    List<dynamic> list;
    List<dynamic> summarytier;
    Summary summary;
    int total;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        list: List<dynamic>.from(json["list"].map((x) => x)),
        summarytier: List<dynamic>.from(json["summarytier"].map((x) => x)),
        summary: Summary.fromJson(json["summary"]),
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "list": List<dynamic>.from(list.map((x) => x)),
        "summarytier": List<dynamic>.from(summarytier.map((x) => x)),
        "summary": summary.toJson(),
        "total": total,
    };
}

class Summary {
    Summary({
        this.totaltransaction,
        this.totalpoint,
        this.remainingpoint,
        this.lifetimevalue,
    });

    int totaltransaction;
    int totalpoint;
    int remainingpoint;
    int lifetimevalue;

    factory Summary.fromJson(Map<String, dynamic> json) => Summary(
        totaltransaction: json["totaltransaction"],
        totalpoint: json["totalpoint"],
        remainingpoint: json["remainingpoint"],
        lifetimevalue: json["lifetimevalue"],
    );

    Map<String, dynamic> toJson() => {
        "totaltransaction": totaltransaction,
        "totalpoint": totalpoint,
        "remainingpoint": remainingpoint,
        "lifetimevalue": lifetimevalue,
    };
}
