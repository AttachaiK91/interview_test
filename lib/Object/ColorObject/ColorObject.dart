import 'package:flutter/material.dart';

final ColorTheme = new ColorTheme2();


class ColorTheme2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        accentColor: Colors.red[500],
        primaryTextTheme: TextTheme(
          headline1: TextStyle(color:Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        
      ),
    );
  }
}
