import 'package:flutter/material.dart';
import 'package:test_interview/Body/MainBody/BuildButtomTab.dart';
import 'package:test_interview/Body/MainBody/ColoumName.dart';
import 'package:test_interview/Body/MainBody/RowTitle.dart';
import 'package:test_interview/Body/ShowDate.dart';
import 'package:test_interview/Body/Summary/PreSummery.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.minWidth > 600) {
        return buildTabletLayout(context);
      } else {
        return buildPhoneLayout(context);
      }
    });
  }

  Widget buildTabletLayout(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text("SMILE SME")),
        body: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 20.0),
                SizedBox(height: 50.0, child: ShowDate()),
                SizedBox(height: 120.0, child: PreSummary()),
                Padding(
                  padding: const EdgeInsets.only(top:2.0),
                  child: ColName(),
                ),
                SizedBox(height: 50.0, child: BuildBottomTab()),

                //PreSummary(),
              ],
            ),
          ],
        ));
  }

  Widget buildPhoneLayout(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text("SMILE SME")),
        body: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 20.0, child: Text("asd")),
                SizedBox(height: 50.0, child: ShowDate()),
                SizedBox(height: 120.0, child: PreSummary()),
                Padding(
                  padding: const EdgeInsets.only(top:2.0),
                  child: ColName(),
                ),
                 SizedBox(height: 50.0, child:BuildBottomTab(),),
                

                //PreSummary(),
              ],
            ),
          ],
        ));
  }
}
